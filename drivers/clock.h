//
// Created by michal on 22.3.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <system/type.h>

// TODO
#define XTL_CLK 40000000

typedef enum
{
                                // PLL_CLK = 240 MHz
                                // RTC_8M_CLK = 8 MHz
    CLOCK_CPU_SOURCE_XTL,       // XTL_CLK / (APB_CTRL_PRE_DIV_CNT + 1)
    CLOCK_CPU_SOURCE_PLL_4,     // PLL_CLK / 4
    CLOCK_CPU_SOURCE_PLL_2,     // PLL_CLK / 2
    CLOCK_CPU_SOURCE_PLL,       // PLL_CLK
    CLOCK_CPU_SOURCE_RTC_8M,    // RTC_8M_CLK / (APB_CTRL_PRE_DIV_CNT + 1)
    CLOCK_CPU_SOURCE_APLL_4,    // APLL_CLK / 4
    CLOCK_CPU_SOURCE_APLL_2     // APLL_CLK / 2
} Clock_CPU_Source_t;

typedef enum
{
    CLOCK_RTC_SLOW_SOURCE_RTC_150K,
    CLOCK_RTC_SLOW_SOURCE_XTL_32K,
    CLOCK_RTC_SLOW_SOURCE_RTC_8M_D256
} Clock_RTC_Slow_Source_t;

typedef enum
{
    CLOCK_RTC_FAST_SOURCE_XTL_4,
    CLOCK_RTC_FAST_SOURCE_RTC_8M
} Clock_RTC_Fast_Source_t;

typedef enum
{
    CLOCK_REGULATOR_VOLTAGE_0V90 = 0,
    CLOCK_REGULATOR_VOLTAGE_0V95,
    CLOCK_REGULATOR_VOLTAGE_1V00,
    CLOCK_REGULATOR_VOLTAGE_1V05,
    CLOCK_REGULATOR_VOLTAGE_1V10,
    CLOCK_REGULATOR_VOLTAGE_1V15,
    CLOCK_REGULATOR_VOLTAGE_1V20,
    CLOCK_REGULATOR_VOLTAGE_1V25
} Clock_Regulator_Voltage_t;

typedef struct
{
    Clock_CPU_Source_t cpu_source;
    Clock_RTC_Fast_Source_t rtc_fast_source;
    Clock_RTC_Slow_Source_t rtc_slow_source;
    uint16_t APB_divider;
} Clock_Config_t;

IRAM_ATTR void Clock_Configure(const Clock_Config_t * clock_config);

IRAM_ATTR uint32_t Clock_Get_CPU_Frequency();

IRAM_ATTR uint32_t Clock_Get_APB_Frequency();


#ifdef __cplusplus
}
#endif
