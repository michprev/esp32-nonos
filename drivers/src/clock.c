//
// Created by michal on 22.3.18.
//

#include <system/type.h>
#include <drivers/clock.h>
#include <registers/apb_reg.h>
#include <registers/rtc_reg.h>
#include <registers/dport_reg.h>
#include <registers/timer_reg.h>
#include <rom/rom.h>
#include <stdbool.h>

// Copyright 2010-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**************************************************************************************
  *                                       Note:                                       *
  *       Some Rtc memory and registers are used, in ROM or in internal library.      *
  *          Please do not use reserved or used rtc memory or registers.              *
  *                                                                                   *
  *************************************************************************************
  *                          RTC  Memory & Store Register usage
  *************************************************************************************
  *     rtc memory addr         type    size            usage
  *     0x3ff61000(0x50000000)  Slow    SIZE_CP         Co-Processor code/Reset Entry
  *     0x3ff61000+SIZE_CP      Slow    4096-SIZE_CP
  *     0x3ff62800              Slow    4096            Reserved
  *
  *     0x3ff80000(0x400c0000)  Fast    8192            deep sleep entry code
  *
  *************************************************************************************
  *     RTC store registers     usage
  *     RTC_CNTL_STORE0_REG     Reserved
  *     RTC_CNTL_STORE1_REG     RTC_SLOW_CLK calibration value
  *     RTC_CNTL_STORE2_REG     Boot time, low word
  *     RTC_CNTL_STORE3_REG     Boot time, high word
  *     RTC_CNTL_STORE4_REG     External XTAL frequency
  *     RTC_CNTL_STORE5_REG     APB bus frequency
  *     RTC_CNTL_STORE6_REG     FAST_RTC_MEMORY_ENTRY
  *     RTC_CNTL_STORE7_REG     FAST_RTC_MEMORY_CRC
  *************************************************************************************
  */

// g_ticks_us defined in ROMs for PRO and APP CPU
/* scale factors used by ets_delay_us */
extern uint32_t g_ticks_per_us_pro;
extern uint32_t g_ticks_per_us_app;

static uint32_t apb_frequency = 0;
static uint32_t cpu_frequency = 0;

static bool xtl_32k_calibrated = false;
static bool rtc_150k_calibrated = false;
static bool rtc_8m_calibrated = false;


/* TODO */

typedef enum {
    RTC_CAL_RTC_MUX = 0,       //!< Currently selected RTC SLOW_CLK
    RTC_CAL_8MD256 = 1,        //!< Internal 8 MHz RC oscillator, divided by 256
    RTC_CAL_32K_XTAL = 2       //!< External 32 kHz XTAL
} rtc_cal_sel_t;

/* TODO */

/*static void Clock_Calibrate()
{

}*/

static void Clock_PLL_Configure(Clock_CPU_Source_t cpu_source)
{
    uint8_t div_ref;
    uint8_t div7_0 = 0;
    uint8_t div10_8;
    uint8_t lref;
    uint8_t dcur;
    uint8_t bw;

    switch (cpu_source)
    {
        case CLOCK_CPU_SOURCE_XTL:
        case CLOCK_CPU_SOURCE_RTC_8M:
        case CLOCK_CPU_SOURCE_APLL_2:
        case CLOCK_CPU_SOURCE_APLL_4:
            break;

        case CLOCK_CPU_SOURCE_PLL:
            RTC.cntl_vreg.dig_dbias_wak = CLOCK_REGULATOR_VOLTAGE_1V20;

            ROM_ets_delay_us(3);

            if (XTL_CLK == 24000000)
            {
                div_ref = 11;
                div7_0 = 144;
                div10_8 = 4;
                lref = 1;
                dcur = 0;
                bw = 1;
            } else if (XTL_CLK == 26000000)
            {
                div_ref = 12;
                div7_0 = 144;
                div10_8 = 4;
                lref = 1;
                dcur = 0;
                bw = 1;
            } else if (XTL_CLK == 40000000)
            {
                div_ref = 0;
                div7_0 = 28;
                div10_8 = 0;
                lref = 0;
                dcur = 6;
                bw = 3;
            }

            ROM_internal_i2c_writeReg(0x66, 4, 11, 0xc3);
            ROM_internal_i2c_writeReg(0x66, 4, 9, 0x74);
            //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_ENDIV5, BBPLL_ENDIV5_VAL_480M);
            //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_BBADC_DSMP, BBPLL_BBADC_DSMP_VAL_480M);
            break;
        case CLOCK_CPU_SOURCE_PLL_2:
        case CLOCK_CPU_SOURCE_PLL_4:
            RTC.cntl_vreg.dig_dbias_wak = CLOCK_REGULATOR_VOLTAGE_1V10;

            if (XTL_CLK == 24000000)
            {
                div_ref = 11;
                div7_0 = 224;
                div10_8 = 4;
                lref = 1;
                dcur = 0;
                bw = 1;
            } else if (XTL_CLK == 26000000)
            {
                div_ref = 12;
                div7_0 = 224;
                div10_8 = 4;
                lref = 1;
                dcur = 0;
                bw = 1;
            } else if (XTL_CLK == 40000000)
            {
                div_ref = 0;
                div7_0 = 32;
                div10_8 = 0;
                lref = 0;
                dcur = 6;
                bw = 3;
            }

            ROM_internal_i2c_writeReg(0x66, 4, 11, 0x43);
            ROM_internal_i2c_writeReg(0x66, 4, 9, 0x84);
            //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_ENDIV5, BBPLL_ENDIV5_VAL_320M);
            //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_BBADC_DSMP, BBPLL_BBADC_DSMP_VAL_320M);
            break;
    }

    uint8_t i2c_bbpll_lref  = (lref << 7) | (div10_8 << 4) | (div_ref);
    uint8_t i2c_bbpll_div_7_0 = div7_0;
    uint8_t i2c_bbpll_dcur = (bw << 6) | dcur;


    // TODO
    ROM_internal_i2c_writeReg(0x66, 4, 2, i2c_bbpll_lref);
    ROM_internal_i2c_writeReg(0x66, 4, 3, i2c_bbpll_div_7_0);
    ROM_internal_i2c_writeReg(0x66, 4, 5, i2c_bbpll_dcur);
    //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_OC_LREF, i2c_bbpll_lref);
    //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_OC_DIV_7_0, i2c_bbpll_div_7_0);
    //I2C_WRITEREG_RTC(I2C_BBPLL, I2C_BBPLL_OC_DCUR, i2c_bbpll_dcur);

    // TODO
    ROM_ets_delay_us(160);
}

uint32_t Clock_Get_CPU_Frequency()
{
    return cpu_frequency;
}

uint32_t Clock_Get_APB_Frequency()
{
    return apb_frequency;
}

static void Clock_Set_CPU_Frequency(uint32_t frequency)
{
    cpu_frequency = frequency;
    g_ticks_per_us_pro = cpu_frequency / 1000000;
    g_ticks_per_us_app = cpu_frequency / 1000000;
}

// for compatibility with ROM / libs code
static void Clock_Store_APB_Frequency()
{
    // ??
    uint8_t tmp_apb = apb_frequency >> 12;

    // same the value in both upper and lower 16 bits
    RTC.cntl_store5.store = (tmp_apb & 0xFFFF) | ((tmp_apb & 0xFFFF) << 16);
}

static void Clock_Set_APB_Frequency(uint32_t frequency)
{
    apb_frequency = frequency;
    Clock_Store_APB_Frequency();
}

// for compatibility with ROM / libs code
static void Clock_Store_XTL_Frequency()
{
    uint8_t xtl_mhz = XTL_CLK / 1000000;

    // same the value in both upper and lower 16 bits
    RTC.cntl_store4.store = (xtl_mhz & 0xFFFF) | ((xtl_mhz & 0xFFFF) << 16);
}

static void Clock_Wait_Slow_Cycle()
{
    // start calibration for 0 slow clock cycles = get nearest slow clock tick
    TIMG0.rtc_cali_cfg.start_cycling = 0;
    TIMG0.rtc_cali_cfg.start = 0;
    TIMG0.rtc_cali_cfg.clk_sel = RTC_CAL_RTC_MUX;
    TIMG0.rtc_cali_cfg.max = 0;
    TIMG0.rtc_cali_cfg.start = 1;

    while (!TIMG0.rtc_cali_cfg.rdy);
}

static void Clock_Set_CPU_Source(Clock_CPU_Source_t source, uint16_t APB_divider)
{
    /* first switch to XTAL */
    RTC.cntl_vreg.dig_dbias_wak = CLOCK_REGULATOR_VOLTAGE_1V10;
    RTC.cntl_clk_conf.soc_clk_sel = 0;
    APB.sysclk_conf.pre_div_cnt = 0;
    Clock_Set_CPU_Frequency(XTL_CLK);

    /* Frequency switch is synchronized to SLOW_CLK cycle. Wait until the switch
     * is complete before disabling the PLL.
     */
    Clock_Wait_Slow_Cycle();

    // TODO what if I2S uses PLL_D2_CLK ??
    // power down PLL
    RTC.cntl_options0.bb_i2c_force_pd = 1;
    RTC.cntl_options0.bbpll_force_pd = 1;
    RTC.cntl_options0.bbpll_i2c_force_pd = 1;

    // if PLLA is under power down then power down also internal I2C
    if (RTC.cntl_ana_conf.cntl_plla_force_pd)
        RTC.cntl_options0.bias_i2c_force_pd = 1;

    Clock_Set_APB_Frequency(XTL_CLK);

    switch (source)
    {
        case CLOCK_CPU_SOURCE_XTL:
            APB.sysclk_conf.pre_div_cnt = APB_divider - 1;
            Clock_Set_CPU_Frequency(XTL_CLK / APB_divider);
            Clock_Set_APB_Frequency(XTL_CLK / APB_divider);

            break;
        case CLOCK_CPU_SOURCE_PLL:
            // power up PLL
            RTC.cntl_options0.bb_i2c_force_pd = 0;
            RTC.cntl_options0.bbpll_force_pd = 0;
            RTC.cntl_options0.bbpll_i2c_force_pd = 0;
            RTC.cntl_options0.bias_i2c_force_pd = 0;

            Clock_PLL_Configure(source);
            // TODO check in eFuse if CPU is rated for 240 MHz

            DPORT.cpu_per_conf.cpu_period_sel = 2;
            RTC.cntl_clk_conf.soc_clk_sel = 1;

            Clock_Set_CPU_Frequency(240000000);
            Clock_Set_APB_Frequency(80000000);
            break;
        case CLOCK_CPU_SOURCE_PLL_2:
            // power up PLL
            RTC.cntl_options0.bb_i2c_force_pd = 0;
            RTC.cntl_options0.bbpll_force_pd = 0;
            RTC.cntl_options0.bbpll_i2c_force_pd = 0;
            RTC.cntl_options0.bias_i2c_force_pd = 0;

            Clock_PLL_Configure(source);

            DPORT.cpu_per_conf.cpu_period_sel = 1;
            RTC.cntl_clk_conf.soc_clk_sel = 1;

            Clock_Set_CPU_Frequency(160000000);
            Clock_Set_APB_Frequency(80000000);
            break;
        case CLOCK_CPU_SOURCE_PLL_4:
            // power up PLL
            RTC.cntl_options0.bb_i2c_force_pd = 0;
            RTC.cntl_options0.bbpll_force_pd = 0;
            RTC.cntl_options0.bbpll_i2c_force_pd = 0;
            RTC.cntl_options0.bias_i2c_force_pd = 0;

            Clock_PLL_Configure(source);

            DPORT.cpu_per_conf.cpu_period_sel = 0;
            RTC.cntl_clk_conf.soc_clk_sel = 1;

            Clock_Set_CPU_Frequency(80000000);
            Clock_Set_APB_Frequency(80000000);
            break;
        case CLOCK_CPU_SOURCE_RTC_8M:
            // TODO
            break;
        case CLOCK_CPU_SOURCE_APLL_2:
            // TODO
            break;
        case CLOCK_CPU_SOURCE_APLL_4:
            // TODO
            break;
    }

    Clock_Wait_Slow_Cycle();
}

/* ------------------------------------------------------------------------------------------------------------------ */

void Clock_Configure(const Clock_Config_t * clock_config)
{
    Clock_Store_XTL_Frequency();

    // TODO calibrate RTC_8M and RTC_150K and XTL32K
    // TODO APLL

    Clock_Set_CPU_Source(clock_config->cpu_source, clock_config->APB_divider);
}
