//
// Created by michal on 7.4.18.
//

#include <registers/dport_reg.h>
#include <registers/rtc_reg.h>
#include <registers/timer_reg.h>
#include <drivers/clock.h>
#include <rom/include/cache.h>
#include <rom/rom.h>
#include <system/type.h>
#include <system/system_time.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void ROM_abort();

void ROM_uart_tx_wait_idle(uint8_t uart_no);

void ROM_uartAttach(void);

void ROM_ets_install_uart_printf(void);

void ROM_uart_div_modify(uint8_t uart_no, uint32_t DivLatchValue);


IRAM_ATTR void tmpfunc()
{
    uint32_t __interrupt = 1 << 7;
    __asm__ __volatile__("wsr.intenable %0" :: "a"(__interrupt):"memory");
    __asm__ __volatile__("wsr.intset %0" :: "a"(__interrupt):"memory");
}

/*void abort()
{
    *((int *) 0) = 0;
}*/



extern int _bss_start;
extern int _bss_end;

void bootloader_flash_init();

IRAM_ATTR static void bootloader_uart_init();


static struct _reent global_reent;


IRAM_ATTR void interrupt_handler(void)
{
    ROM_ets_printf("here comes the interrupt 3\n");
}

IRAM_ATTR void interrupt_handler2(void)
{
    uint32_t __interrupt = 1 << 29;
    __asm__ __volatile__("wsr.intset %0" :: "a"(__interrupt):"memory");
    ROM_ets_printf("here comes the interrupt 1\n");
}


IRAM_ATTR void bootloader_start()
{
    uint32_t vecbase_address = 0x40080000;
    __asm__ __volatile__ ("wsr.vecbase %0" :: "r"(vecbase_address));

    //Clear bss
    memset(&_bss_start, 0, (&_bss_end - &_bss_start) * sizeof(_bss_start));

    // TODO reverse engineer
    ROM_Cache_Read_Disable(0);
    ROM_Cache_Read_Disable(1);
    ROM_Cache_Flush(0);
    ROM_Cache_Flush(1);

    // workaround for 3.1. Due to the cache MMU bug, a spurious watchdog reset occurs when ESP32 is powered up or
    // wakes up from Deep-sleep.
    ROM_mmu_init(0);
    DPORT.pro_cache_ctrl1.mmu_ia_clr = 1;
    ROM_mmu_init(1);
    DPORT.pro_cache_ctrl1.mmu_ia_clr = 0;

    DPORT.pro_cache_ctrl1.mask_drom0 = 0;
    DPORT.app_cache_ctrl1.mask_drom0 = 0;

    ROM_uart_tx_wait_idle(0);

    Clock_Config_t clock_config;
    clock_config.cpu_source = CLOCK_CPU_SOURCE_PLL;
    Clock_Configure(&clock_config);

    bootloader_uart_init();

    // disable flashboot watchdogs
    RTC.cntl_wdtconfig0.cntl_wdt_flashboot_mod_en = 0;
    TIMG0.wdtconfig0.flashboot_mod_en = 0;


    System_Time_Init(240000);

    while (true)
    {
        extern uint32_t _tmp1;
        extern uint32_t _tmp2;

        ROM_ets_printf("%d %d\n", _tmp1, _tmp2);
    }
}

/*void bootloader_flash_init()
{
    //extern uint8_t g_rom_spiflash_dummy_len_plus[];

    eFuse_Package_t chip_package = eFuse_Get_Package();

    switch (chip_package)
    {
        case EFUSE_PACKAGE_ESP32_D2WDQ5:
        case EFUSE_PACKAGE_ESP32_PICOD2:
        case EFUSE_PACKAGE_ESP32_PICOD4:
        {
            // SPI pins already configured from ROM bootloader
            // ensure that SPI_CLK is directed from IO_MUX
            IO_MUX_Config_t spi_clk_config;
            spi_clk_config.function = IO_MUX_FUNC_SPI_CLK;
            spi_clk_config.drive = IO_MUX_DRIVE_2;
            spi_clk_config.pull = IO_MUX_PULL_NO;
            spi_clk_config.input_enable = false;

            IO_MUX_Config(&spi_clk_config);
            break;
        }
        default:
        {
            // TODO get SPI config from eFuse
        }
    }
}*/

void bootloader_uart_init()
{
    ROM_uartAttach();
    ROM_ets_install_uart_printf();

    // Wait for UART FIFO to be empty.
    ROM_uart_tx_wait_idle(0);

    // Set configured UART console baud rate
    const int uart_baud = 115200;
    ROM_uart_div_modify(0, (Clock_Get_APB_Frequency() << 4) / uart_baud);
}
