//
// Created by michal on 20.3.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>
#include <assert.h>

#define assert_param(param)

#define IRAM_ATTR   __attribute__((section(".iram")))
#define DRAM_ATTR   __attribute__((section(".dram")))

#define MAX_DELAY   0xFFFFFFFFU

typedef enum
{
    STATUS_OK,
    STATUS_ERROR,
    STATUS_BUSY,
    STATUS_TIMEOUT
} Status_t;

#define CORE_ID_PRO 0xCDCD
#define CORE_ID_APP 0xABAB

IRAM_ATTR static inline uint32_t GET_CORE_ID(void)
{
    uint32_t id;
    __asm__ __volatile__ (
        "rsr.prid %0\n"
        :"=r"(id)
    );
    return id;
}

IRAM_ATTR static inline uint8_t GET_CORE_INDEX(void)
{
    uint32_t id;
    __asm__ __volatile__ (
        "rsr.prid %0\n"
        " extui %0,%0,13,1"
        :"=r"(id)
    );
    return id;
}

#ifdef __cplusplus
}
#endif