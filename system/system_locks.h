//
// Created by michal on 22.4.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
#include <system/type.h>

/**
 * Value defining mutex without an owner
 * Should not be needed to be used
 */
#define MUTEX_NO_OWNER  0xB33FFFFF

/**
 * Initializer of a mutex
 * Please use this everytime declaring a mutex
 */
#define MUTEX_UNLOCKED  MUTEX_NO_OWNER

/**
 * Initializer of a recursive mutex
 * Please use this everytime declaring a recursive mutex
 */
#define RECURSIVE_MUTEX_UNLOCKED {	\
    .owner = MUTEX_NO_OWNER,	    \
    .count = 0,					    \
}

/**
 * Mutex handle typedef
 * Value MUTEX_NO_OWNER represents unlocked mutex
 * Value CORE_ID_PRO represents mutex locked by PRO core
 * Value CORE_ID_APP represents mutex locked by APP core
 */
typedef uint32_t Mutex_t;

/**
 * Recursive mutex handle typedef
 */
typedef struct
{
    /**
     * Value MUTEX_NO_OWNER represents unlocked mutex
     * Value CORE_ID_PRO represents mutex locked by PRO core
     * Value CORE_ID_APP represents mutex locked by APP core
     */
    uint32_t owner;
    /**
     * Number of recursive locks
     */
    uint8_t count;
} Recursive_Mutex_t;

/**
 * Enter a critical section only on the calling core
 * Disable interrupts on the calling core
 * May be called multiple times
 */
IRAM_ATTR void ENTER_CRITICAL_SINGLE_CORE(void);

/**
 * Exit a critical section only on the calling core
 * Enable interrupts once exited all nested sections
 * May be called multiple times
 */
IRAM_ATTR void EXIT_CRITICAL_SINGLE_CORE(void);

/**
 * Enter a critical section on both cores
 * Disable interrupts on the calling core and acquire section mutex
 * May be called multiple times
 * @param mutex section mutex to disallow acquiring of the section by the second core
 */
IRAM_ATTR void ENTER_CRITICAL_DUAL_CORE(Recursive_Mutex_t * mutex);

/**
 * Exit a critical section on both cores
 * Enable interrupts and release mutex once exited all nested sections
 * @param mutex section mutex
 */
IRAM_ATTR void EXIT_CRITICAL_DUAL_CORE(Recursive_Mutex_t * mutex);


/**
 * Mutex init function to be called where MUTEX_UNLOCKED could not be used
 * @param mutex mutex to be initialized
 */
IRAM_ATTR static inline void MUTEX_INIT(Mutex_t * mutex)
{
    *mutex = MUTEX_UNLOCKED;
}

/**
 * Lock the mutex
 * @param mutex mutex to be locked
 */
IRAM_ATTR void MUTEX_LOCK(Mutex_t * mutex);

/**
 * Try to lock the mutex
 * @param mutex mutex to be locked
 * @return true if succeeds, false otherwise
 */
IRAM_ATTR bool MUTEX_TRY_LOCK(Mutex_t * mutex);

/**
 * Unlock the mutex
 * @param mutex mutex to be unlocked
 */
IRAM_ATTR void MUTEX_UNLOCK(Mutex_t * mutex);

/**
 * Recursive mutex init function to be called where RECURSIVE_MUTEX_UNLOCKED could not be used
 * @param mutex mutex to be initialized
 */
IRAM_ATTR static inline void RECURSIVE_MUTEX_INIT(Recursive_Mutex_t * mutex)
{
    mutex->owner = MUTEX_UNLOCKED;
    mutex->count = 0;
}

/**
 * Lock the recursive mutex
 * @param mutex recursive mutex to be locked
 */
IRAM_ATTR void RECURSIVE_MUTEX_LOCK(Recursive_Mutex_t * mutex);

/**
 * Try to lock the recursive mutex
 * @param mutex recursive mutex to be locked
 * @return true if succeeds, false otherwise
 */
IRAM_ATTR bool RECURSIVE_MUTEX_TRY_LOCK(Recursive_Mutex_t * mutex);

/**
 * Unlock the recursive mutex
 * @param mutex recursive mutex to be unlocked
 */
IRAM_ATTR void RECURSIVE_MUTEX_UNLOCK(Recursive_Mutex_t * mutex);

#ifdef __cplusplus
}
#endif