//
// Created by michal on 9.6.18.
//

#include <system/system_time.h>

void System_Time_Init(uint32_t ticks)
{
    extern uint32_t _timer_ccompare;
    _timer_ccompare = ticks;

    extern uint32_t _timer_ticks;
    _timer_ticks = 0;

    uint32_t ccount;
    __asm__ __volatile__ ("rsr.ccount %0" : "=r"(ccount));

    ccount += ticks;
    __asm__ __volatile__ ("wsr.ccompare2 %0" :: "r"(ccount));


    uint32_t intenable;
    __asm__ __volatile__ ("rsr.intenable %0" : "=r"(intenable));

    intenable |= 1u << 16u;

    __asm__ __volatile__ ("wsr.intenable %0" :: "r"(intenable));
}
