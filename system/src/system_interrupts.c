//
// Created by michal on 9.6.18.
//

#include <xtensa/config/core-isa.h>
#include <system/system_locks.h>
#include <system/system_interrupts.h>

typedef struct
{
    uint8_t number;
    Interrupt_Type_t type;
    Interrupt_Priority_t priority;
} Interrupt_Descriptor_t;

DRAM_ATTR const static Interrupt_Descriptor_t interrupt_descriptors[] =
    {
        { 0,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 1,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 2,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 3,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 4,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 5,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 8,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 9,  INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 10, INTERRUPT_TYPE_EDGE,  INTERRUPT_PRIORITY_1 },
        { 12, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 13, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 17, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 18, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_1 },
        { 19, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_2 },
        { 20, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_2 },
        { 21, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_2 },
        { 22, INTERRUPT_TYPE_EDGE,  INTERRUPT_PRIORITY_3 },
        { 23, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_3 },
        { 27, INTERRUPT_TYPE_LEVEL, INTERRUPT_PRIORITY_3 }
    };

extern void * _interrupt_table[XCHAL_NUM_INTERRUPTS * 2];

static Recursive_Mutex_t interrupts_mutex = RECURSIVE_MUTEX_UNLOCKED;


Status_t Interrupt_Register(Interrupt_Source_t source, Interrupt_Callback_t callback, Interrupt_Type_t type,
                            Interrupt_Priority_t priority)
{
    assert_param(IS_INTERRUPT_SOURCE(source));
    assert_param(callback != NULL);
    assert_param(IS_INTERRUPT_TYPE(type));
    assert_param(IS_INTERRUPT_PRIORITY(priority));

    ENTER_CRITICAL_DUAL_CORE(&interrupts_mutex);

    uint8_t offset = GET_CORE_INDEX() * XCHAL_NUM_INTERRUPTS;

    for (uint8_t i = 0; i < sizeof(interrupt_descriptors); i++)
    {
        if (interrupt_descriptors[i].priority == priority &&
            interrupt_descriptors[i].type == type &&
            _interrupt_table[interrupt_descriptors[i].number] == NULL)
        {
            uint8_t int_number = interrupt_descriptors[i].number;
            _interrupt_table[int_number] = callback;

            switch (GET_CORE_ID())
            {
                case CORE_ID_PRO:
                    *((volatile uint32_t *)(0x3FF00104 + 4 * source)) = int_number;
                    break;
                case CORE_ID_APP:
                    *((volatile uint32_t *)0x3FF00218 + 4 * source) = int_number;
                    break;
            }

            if (type == INTERRUPT_TYPE_EDGE)
            {
                uint32_t intclear = 1u << int_number;
                __asm__ __volatile__ ("wsr.intclear %0"::"r"(intclear));
            }

            uint32_t intenable;
            __asm__ __volatile__ ("rsr.intenable %0" : "=r"(intenable));

            intenable |= 1u << int_number;

            __asm__ __volatile__ ("wsr.intenable %0" :: "r"(intenable));

            EXIT_CRITICAL_DUAL_CORE(&interrupts_mutex);

            return STATUS_OK;
        }
    }

    EXIT_CRITICAL_DUAL_CORE(&interrupts_mutex);

    return STATUS_ERROR;
}

void Interrupt_Free(Interrupt_Source_t source)
{
    assert_param(IS_INTERRUPT_SOURCE(source));

    ENTER_CRITICAL_DUAL_CORE(&interrupts_mutex);

    uint8_t int_number;

    switch (GET_CORE_ID())
    {
        case CORE_ID_PRO:
            int_number = *((volatile uint32_t *)(0x3FF00104 + 4 * source)) & 0x1F;
            *((volatile uint32_t *)(0x3FF00104 + 4 * source)) = 16;
            break;
        case CORE_ID_APP:
            int_number = *((volatile uint32_t *)(0x3FF00218 + 4 * source)) & 0x1F;
            *((volatile uint32_t *)(0x3FF00218 + 4 * source)) = 16;
            break;
    }

    // in case that int_number is peripheral interrupt clear it in interrupt table and disable that interrupt
    if ((1u << int_number) & 0x8FE373Fu)
    {
        _interrupt_table[int_number] = NULL;

        uint32_t intenable;
        __asm__ __volatile__ ("rsr.intenable %0" : "=r"(intenable));

        intenable &= ~(1u << int_number);

        __asm__ __volatile__ ("wsr.intenable %0"::"r"(intenable));
    }

    EXIT_CRITICAL_DUAL_CORE(&interrupts_mutex);
}

