//
// Created by michal on 9.6.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <system/type.h>

/**
 * Initialize system timer
 * Will enable level 5 timer 2 interrupt
 * @param ticks amount of CPU ticks between two timer ticks
 */
IRAM_ATTR void System_Time_Init(uint32_t ticks);

/**
 * @return number of system timer ticks
 */
IRAM_ATTR static inline uint32_t System_Time_Get_Ticks(void)
{
    extern uint32_t _timer_ticks;

    return _timer_ticks;
}

#ifdef __cplusplus
}
#endif