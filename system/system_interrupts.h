//
// Created by michal on 9.6.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "type.h"

#define IS_INTERRUPT_TYPE(TYPE)         (((TYPE) == INTERRUPT_TYPE_LEVEL) || ((TYPE) == INTERRUPT_TYPE_EDGE))
#define IS_INTERRUPT_PRIORITY(PRIORITY) (((PRIORITY) >= INTERRUPT_PRIORITY_1) && ((PRIORITY) <= INTERRUPT_PRIORITY_3))
#define IS_INTERRUPT_SOURCE(SOURCE)     (((SOURCE) >= INTERRUPT_SOURCE_WIFI_MAC) && \
                                         ((SOURCE) <= INTERRUPT_SOURCE_CACHE_IA))

typedef enum
{
    INTERRUPT_TYPE_LEVEL,
    INTERRUPT_TYPE_EDGE
} Interrupt_Type_t;

typedef enum
{
    INTERRUPT_PRIORITY_1,
    INTERRUPT_PRIORITY_2,
    INTERRUPT_PRIORITY_3
} Interrupt_Priority_t;

typedef enum
{
    INTERRUPT_SOURCE_WIFI_MAC = 0,     /**< interrupt of WiFi MAC, level*/
    INTERRUPT_SOURCE_WIFI_MAC_NMI = 1,     /**< interrupt of WiFi MAC, NMI, use if MAC have bug to fix in NMI*/
    INTERRUPT_SOURCE_WIFI_BB = 2,     /**< interrupt of WiFi BB, level, we can do some calibartion*/
    INTERRUPT_SOURCE_BT_MAC = 3,     /**< will be cancelled*/
    INTERRUPT_SOURCE_BT_BB = 4,     /**< interrupt of BT BB, level*/
    INTERRUPT_SOURCE_BT_BB_NMI = 5,     /**< interrupt of BT BB, NMI, use if BB have bug to fix in NMI*/
    INTERRUPT_SOURCE_RWBT = 6,     /**< interrupt of RWBT, level*/
    INTERRUPT_SOURCE_RWBLE = 7,     /**< interrupt of RWBLE, level*/
    INTERRUPT_SOURCE_RWBT_NMI = 8,     /**< interrupt of RWBT, NMI, use if RWBT have bug to fix in NMI*/
    INTERRUPT_SOURCE_RWBLE_NMI = 9,     /**< interrupt of RWBLE, NMI, use if RWBT have bug to fix in NMI*/
    INTERRUPT_SOURCE_SLC0 = 10,    /**< interrupt of SLC0, level*/
    INTERRUPT_SOURCE_SLC1 = 11,    /**< interrupt of SLC1, level*/
    INTERRUPT_SOURCE_UHCI0 = 12,    /**< interrupt of UHCI0, level*/
    INTERRUPT_SOURCE_UHCI1 = 13,    /**< interrupt of UHCI1, level*/
    INTERRUPT_SOURCE_TG0_T0_LEVEL = 14,    /**< interrupt of TIMER_GROUP0, TIMER0, level, we would like use EDGE for timer if permission*/
    INTERRUPT_SOURCE_TG0_T1_LEVEL = 15,    /**< interrupt of TIMER_GROUP0, TIMER1, level, we would like use EDGE for timer if permission*/
    INTERRUPT_SOURCE_TG0_WDT_LEVEL = 16,    /**< interrupt of TIMER_GROUP0, WATCHDOG, level*/
    INTERRUPT_SOURCE_TG0_LACT_LEVEL = 17,    /**< interrupt of TIMER_GROUP0, LACT, level*/
    INTERRUPT_SOURCE_TG1_T0_LEVEL = 18,    /**< interrupt of TIMER_GROUP1, TIMER0, level, we would like use EDGE for timer if permission*/
    INTERRUPT_SOURCE_TG1_T1_LEVEL = 19,    /**< interrupt of TIMER_GROUP1, TIMER1, level, we would like use EDGE for timer if permission*/
    INTERRUPT_SOURCE_TG1_WDT_LEVEL = 20,    /**< interrupt of TIMER_GROUP1, WATCHDOG, level*/
    INTERRUPT_SOURCE_TG1_LACT_LEVEL = 21,    /**< interrupt of TIMER_GROUP1, LACT, level*/
    INTERRUPT_SOURCE_GPIO = 22,    /**< interrupt of GPIO, level*/
    INTERRUPT_SOURCE_GPIO_NMI = 23,    /**< interrupt of GPIO, NMI*/
    INTERRUPT_SOURCE_FROM_CPU_0 = 24,    /**< interrupt0 generated from a CPU, level*/
    INTERRUPT_SOURCE_FROM_CPU_1 = 25,    /**< interrupt1 generated from a CPU, level*/
    INTERRUPT_SOURCE_FROM_CPU_2 = 26,    /**< interrupt2 generated from a CPU, level*/
    INTERRUPT_SOURCE_FROM_CPU_3 = 27,    /**< interrupt3 generated from a CPU, level*/
    INTERRUPT_SOURCE_SPI0 = 28,    /**< interrupt of SPI0, level, SPI0 is for Cache Access, do not use this*/
    INTERRUPT_SOURCE_SPI1 = 29,    /**< interrupt of SPI1, level, SPI1 is for flash read/write, do not use this*/
    INTERRUPT_SOURCE_SPI2 = 30,    /**< interrupt of SPI2, level*/
    INTERRUPT_SOURCE_SPI3 = 31,    /**< interrupt of SPI3, level*/
    INTERRUPT_SOURCE_I2S0 = 32,    /**< interrupt of I2S0, level*/
    INTERRUPT_SOURCE_I2S1 = 33,    /**< interrupt of I2S1, level*/
    INTERRUPT_SOURCE_UART0 = 34,    /**< interrupt of UART0, level*/
    INTERRUPT_SOURCE_UART1 = 35,    /**< interrupt of UART1, level*/
    INTERRUPT_SOURCE_UART2 = 36,    /**< interrupt of UART2, level*/
    INTERRUPT_SOURCE_SDIO_HOST = 37,    /**< interrupt of SD/SDIO/MMC HOST, level*/
    INTERRUPT_SOURCE_ETH_MAC = 38,    /**< interrupt of ethernet mac, level*/
    INTERRUPT_SOURCE_PWM0 = 39,    /**< interrupt of PWM0, level, Reserved*/
    INTERRUPT_SOURCE_PWM1 = 40,    /**< interrupt of PWM1, level, Reserved*/
    INTERRUPT_SOURCE_PWM2 = 41,    /**< interrupt of PWM2, level*/
    INTERRUPT_SOURCE_PWM3 = 42,    /**< interruot of PWM3, level*/
    INTERRUPT_SOURCE_LEDC = 43,    /**< interrupt of LED PWM, level*/
    INTERRUPT_SOURCE_EFUSE = 44,    /**< interrupt of efuse, level, not likely to use*/
    INTERRUPT_SOURCE_CAN = 45,    /**< interrupt of can, level*/
    INTERRUPT_SOURCE_RTC_CORE = 46,    /**< interrupt of rtc core, level, include rtc watchdog*/
    INTERRUPT_SOURCE_RMT = 47,    /**< interrupt of remote controller, level*/
    INTERRUPT_SOURCE_PCNT = 48,    /**< interrupt of pluse count, level*/
    INTERRUPT_SOURCE_I2C_EXT0 = 49,    /**< interrupt of I2C controller1, level*/
    INTERRUPT_SOURCE_I2C_EXT1 = 50,    /**< interrupt of I2C controller0, level*/
    INTERRUPT_SOURCE_RSA = 51,    /**< interrupt of RSA accelerator, level*/
    INTERRUPT_SOURCE_SPI1_DMA = 52,    /**< interrupt of SPI1 DMA, SPI1 is for flash read/write, do not use this*/
    INTERRUPT_SOURCE_SPI2_DMA = 53,    /**< interrupt of SPI2 DMA, level*/
    INTERRUPT_SOURCE_SPI3_DMA = 54,    /**< interrupt of SPI3 DMA, level*/
    INTERRUPT_SOURCE_WDT = 55,    /**< will be cancelled*/
    INTERRUPT_SOURCE_TIMER1 = 56,    /**< will be cancelled*/
    INTERRUPT_SOURCE_TIMER2 = 57,    /**< will be cancelled*/
    INTERRUPT_SOURCE_TG0_T0_EDGE = 58,    /**< interrupt of TIMER_GROUP0, TIMER0, EDGE*/
    INTERRUPT_SOURCE_TG0_T1_EDGE = 59,    /**< interrupt of TIMER_GROUP0, TIMER1, EDGE*/
    INTERRUPT_SOURCE_TG0_WDT_EDGE = 60,    /**< interrupt of TIMER_GROUP0, WATCH DOG, EDGE*/
    INTERRUPT_SOURCE_TG0_LACT_EDGE = 61,    /**< interrupt of TIMER_GROUP0, LACT, EDGE*/
    INTERRUPT_SOURCE_TG1_T0_EDGE = 62,    /**< interrupt of TIMER_GROUP1, TIMER0, EDGE*/
    INTERRUPT_SOURCE_TG1_T1_EDGE = 63,    /**< interrupt of TIMER_GROUP1, TIMER1, EDGE*/
    INTERRUPT_SOURCE_TG1_WDT_EDGE = 64,    /**< interrupt of TIMER_GROUP1, WATCHDOG, EDGE*/
    INTERRUPT_SOURCE_TG1_LACT_EDGE = 65,    /**< interrupt of TIMER_GROUP0, LACT, EDGE*/
    INTERRUPT_SOURCE_MMU_IA = 66,    /**< interrupt of MMU Invalid Access, LEVEL*/
    INTERRUPT_SOURCE_MPU_IA = 67,    /**< interrupt of MPU Invalid Access, LEVEL*/
    INTERRUPT_SOURCE_CACHE_IA = 68     /**< interrupt of Cache Invalied Access, LEVEL*/
} Interrupt_Source_t;

typedef void (* Interrupt_Callback_t)(void);

IRAM_ATTR Status_t Interrupt_Register(Interrupt_Source_t source, Interrupt_Callback_t callback, Interrupt_Type_t type,
                                      Interrupt_Priority_t priority);

IRAM_ATTR void Interrupt_Free(Interrupt_Source_t source);

#ifdef __cplusplus
}
#endif