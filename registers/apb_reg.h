//
// Created by michal on 22.4.18.
//

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef volatile struct
{
    union
    {
        struct
        {
            uint32_t pre_div_cnt:       10;
            uint32_t clk_320m_en:       1;
            uint32_t clk_en:            1;
            uint32_t rst_tick_cnt:      1;
            uint32_t quick_clk_chng:    1;
            uint32_t :                  18;
        };
        uint32_t value;
    } sysclk_conf;
    // TODO
} apb_t;

extern apb_t APB;

#ifdef __cplusplus
}
#endif
