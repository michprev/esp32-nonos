//
// Created by michal on 20.4.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef volatile struct
{
    struct
    {
        union
        {
            struct
            {
                uint32_t :              10;
                uint32_t alarm_en:      1;
                uint32_t level_int_en:  1;
                uint32_t edge_int_en:   1;
                uint32_t divider:       16;
                uint32_t autoreload:    1;
                uint32_t increase:      1;
                uint32_t en:            1;
            };
            uint32_t value;
        } config;
        uint32_t lo;
        uint32_t hi;
        uint32_t update;
        uint32_t alarm_lo;
        uint32_t alarm_hi;
        uint32_t load_lo;
        uint32_t load_hi;
        uint32_t load;
    } timer[2];
    union
    {
        struct
        {
            uint32_t :                  14;
            uint32_t flashboot_mod_en:  1;
            uint32_t sys_reset_length:  3;
            uint32_t cpu_reset_length:  3;
            uint32_t level_int_en:      1;
            uint32_t edge_int_en:       1;
            uint32_t stg3:              2;
            uint32_t stg2:              2;
            uint32_t stg1:              2;
            uint32_t stg0:              2;
            uint32_t wdt_en:            1;
        };
        uint32_t value;
    } wdtconfig0;
    union
    {
        struct
        {
            uint32_t :              16;
            uint32_t clk_prescale:  16;
        };
        uint32_t value;
    } wdtconfig1;
    union
    {
        struct
        {
            uint32_t stg0_hold: 32;
        };
        uint32_t value;
    } wdtconfig2;
    union
    {
        struct
        {
            uint32_t stg1_hold: 32;
        };
        uint32_t value;
    } wdtconfig3;
    union
    {
        struct
        {
            uint32_t stg2_hold: 32;
        };
        uint32_t value;
    } wdtconfig4;
    union
    {
        struct
        {
            uint32_t stg3_hold: 32;
        };
        uint32_t value;
    } wdtconfig5;
    union
    {
        struct
        {
            uint32_t feed:  32;
        };
        uint32_t value;
    } wdtfeed;
    union
    {
        struct
        {
            uint32_t wkey:  32;
        };
        uint32_t value;
    } wdtwprotect;
    union
    {
        struct
        {
            uint32_t :              12;
            uint32_t start_cycling: 1;
            uint32_t clk_sel:       2;
            uint32_t rdy:           1;
            uint32_t max:           15;
            uint32_t start:         1;
        };
        uint32_t value;
    } rtc_cali_cfg;
    union
    {
        struct
        {
            uint32_t :              7;
            uint32_t cali_value:    25;
        };
        uint32_t value;
    } rtc_cali_cfg1;

    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;
    uint32_t :  32;

    union
    {
        struct
        {
            uint32_t t0:    1;
            uint32_t t1:    1;
            uint32_t wdt:   1;
            uint32_t :      29;
        };
        uint32_t value;
    } int_ena;
    union
    {
        struct
        {
            uint32_t t0:    1;
            uint32_t t1:    1;
            uint32_t wdt:   1;
            uint32_t :      29;
        };
        uint32_t value;
    } int_raw;
    union
    {
        struct
        {
            uint32_t t0:    1;
            uint32_t t1:    1;
            uint32_t wdt:   1;
            uint32_t :      29;
        };
        uint32_t value;
    } int_st;
    union
    {
        struct
        {
            uint32_t t0:    1;
            uint32_t t1:    1;
            uint32_t wdt:   1;
            uint32_t :      29;
        };
        uint32_t value;
    } int_clr;
} Timer_Group_HW_t;

extern Timer_Group_HW_t TIMG0;
extern Timer_Group_HW_t TIMG1;

#ifdef __cplusplus
}
#endif
