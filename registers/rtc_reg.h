//
// Created by michal on 13.4.18.
//

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef volatile struct
{
    union
    {
        struct
        {
            uint32_t sw_stall_appcpu_c0:    2;
            uint32_t sw_stall_procpu_c0:    2;
            uint32_t sw_appcpu_rst:         1;
            uint32_t sw_procpu_rst:         1;
            uint32_t bb_i2c_force_pd:       1;
            uint32_t bb_i2c_force_pu:       1;
            uint32_t bbpll_i2c_force_pd:    1;
            uint32_t bbpll_i2c_force_pu:    1;
            uint32_t bbpll_force_pd:        1;
            uint32_t bbpll_force_pu:        1;
            uint32_t xtl_force_pd:          1;
            uint32_t xtl_force_pu:          1;
            uint32_t bias_sleep_folw_8m:    1;
            uint32_t bias_force_sleep:      1;
            uint32_t bias_force_nosleep:    1;
            uint32_t bias_i2c_folw_8m:      1;
            uint32_t bias_i2c_force_pd:     1;
            uint32_t bias_i2c_force_pu:     1;
            uint32_t bias_core_folw_8m:     1;
            uint32_t bias_core_force_pd:    1;
            uint32_t bias_core_force_pu:    1;
            uint32_t dg_wrap_force_rst:     1;
            uint32_t dg_wrap_force_norst:   1;
            uint32_t sw_sys_rst:            1;
        };
        uint32_t value;
    } cntl_options0;
    union
    {
        struct
        {
            uint32_t cntl_slp_timer0:   32;
        };
        uint32_t value;
    } cntl_slp_timer0;
    union
    {
        struct
        {
            uint32_t cntl_slp_val_hi:           16;
            uint32_t cntl_main_timer_alarm_en:  1;
            uint32_t :                          15;
        };
        uint32_t value;
    } cntl_slp_timer1;
    union
    {
        struct
        {
            uint32_t :      30;
            uint32_t time_valid:    1;
            uint32_t time_update:   1;
        };
        uint32_t value;
    } cntl_time_update;
    union
    {
        struct
        {
            uint32_t cntl_time0:    1;
        };
        uint32_t value;
    } cntl_time0;
    union
    {
        struct
        {
            uint32_t cntl_time1:    1;
        };
        uint32_t value;
    } cntl_time1;
    union
    {
        struct
        {
            uint32_t :                              20;
            uint32_t cntl_touch_wakeup_force_en:    1;
            uint32_t cntl_ulp_cp_wakeup_force_en:   1;
            uint32_t cntl_abp2rtc_bridge_sel:       1;
            uint32_t cntl_touch_slp_timer_en:       1;
            uint32_t cntl_ulp_cp_slp_timer_en:      1;
            uint32_t :                              3;
            uint32_t cntl_sdio_active_ind:          1;
            uint32_t cntl_slp_wakeup:               1;
            uint32_t cntl_slp_reject:               1;
            uint32_t cntl_sleep_en:                 1;
        };
        uint32_t value;
    } cntl_state0;
    union
    {
        struct
        {
            uint32_t cntl_cpu_stall_en:     1;
            uint32_t cntl_cpu_stall_wait:   5;
            uint32_t cntl_ck8m_wait:        8;
            uint32_t cntl_xtl_buf_wait:     10;
            uint32_t cntl_pll_buf_wait:     8;
        };
        uint32_t value;
    } cntl_timer1;
    union
    {
        struct
        {
            uint32_t :                              15;
            uint32_t cntl_ulpcp_touch_start_wait:   9;
            uint32_t cntl_min_time_ck8m_off:        8;
        };
        uint32_t value;
    } cntl_timer2;
    union
    {
        struct
        {
            uint32_t cntl_wifi_wait_timer:          9;
            uint32_t cntl_wifi_powerup_timer:       7;
            uint32_t cntl_rom_ram_wait_timer:       9;
            uint32_t cntl_rom_ram_powerup_timer:    7;
        };
        uint32_t value;
    } cntl_timer3;
    union
    {
        struct
        {
            uint32_t cntl_wait_timer:               9;
            uint32_t cntl_powerup_timer:            7;
            uint32_t cntl_dg_wrap_wait_timer:       9;
            uint32_t cntl_dg_wrap_powerup_timer:    7;
        };
        uint32_t value;
    } cntl_timer4;
    union
    {
        struct
        {
            uint32_t cntl_ulp_cp_subtimer_prediv:   8;
            uint32_t cntl_min_slp_val:              8;
            uint32_t cntl_rtcmem_wait_timer:        9;
            uint32_t cntl_rtcmem_powerup_timer:     7;
        };
        uint32_t value;
    } cntl_timer5;
    union
    {
        struct
        {
            uint32_t :                          23;
            uint32_t cntl_plla_force_pd:        1;
            uint32_t cntl_plla_force_pu:        1;
            uint32_t cntl_bbpll_cal_slp_start:  1;
            uint32_t cntl_pvtmon_pu:            1;
            uint32_t cntl_txrf_i2c_pu:          1;
            uint32_t cntl_rxrf_pbus_pu:         1;
            uint32_t :                          1;
            uint32_t cntl_ckgen_i2c_pu:         1;
            uint32_t cntl_pll_i2c_pu:           1;
        };
        uint32_t value;
    } cntl_ana_conf;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_reset_state;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_wakeup_state;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_int_ena;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_int_raw;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_int_st;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_int_clr;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_store0;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_store1;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_store2;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_store3;
    union
    {
        struct
        {
            uint32_t :          30;
            uint32_t ctr_lv:    1;
            uint32_t ctr_en:    1;
        };
        uint32_t value;
    } cntl_ext_xtl_conf;
    union
    {
        struct
        {
            uint32_t :              30;
            uint32_t wakeup0_lv:    1;
            uint32_t wakeup1_lv:    1;
        };
        uint32_t value;
    } cntl_ext_wakeup_conf;
    union
    {
        struct
        {
            uint32_t :                      24;
            uint32_t gpio_reject_en:        1;
            uint32_t sdio_reject_en:        1;
            uint32_t light_slp_reject_en:   1;
            uint32_t deep_slp_reject_en:    1;
            uint32_t reject_cause:          4;
        };
        uint32_t value;
    } cntl_slp_reject_conf;
    union
    {
        struct
        {
            uint32_t :              29;
            uint32_t cpusel_conf:   1;
            uint32_t cpuperiod_sel: 2;
        };
        uint32_t value;
    } cntl_cpu_period_conf;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_sdio_act_conf;
    union
    {
        struct
        {
            uint32_t :                      4;
            uint32_t ck8m_div:              2;
            uint32_t enb_ck8m:              1;
            uint32_t enb_ck8m_div:          1;
            uint32_t dig_xtal32k_en:        1;
            uint32_t dig_clk8m_d256_en:     1;
            uint32_t dig_clk8m_en:          1;
            uint32_t ck8m_dfreq_force:      1;
            uint32_t ck8m_div_sel:          3;
            uint32_t xtal_force_nogating:   1;
            uint32_t ck8m_force_nogating:   1;
            uint32_t ck8m_dfreq:            8;
            uint32_t ck8m_force_pd:         1;
            uint32_t ck8m_force_pu:         1;
            uint32_t soc_clk_sel:           2;
            uint32_t fast_clk_rtc_sel:      1;
            uint32_t ana_clk_rtc_sel:       2;
        };
        uint32_t value;
    } cntl_clk_conf;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_sdio_conf;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_bias_conf;
    union
    {
        struct
        {
            uint32_t :                  7;
            uint32_t sck_dcap_force:    1;
            uint32_t dig_dbias_slp:     3;
            uint32_t dig_dbias_wak:     3;  /* Digital voltage regulator DBIAS during wake-up. */
            uint32_t sck_dcap:          8;
            uint32_t dbias_slp:         3;
            uint32_t dbias_wak:         3;
            uint32_t dboost_force_pd:   1;
            uint32_t dboost_force_pu:   1;
            uint32_t force_pd:          1;
            uint32_t force_pu:          1;
        };
        uint32_t value;
    } cntl_vreg;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_pwc;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_dig_pwc;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } cntl_dig_iso;
    union
    {
        struct
        {
            uint32_t :                          7;
            uint32_t cntl_wdt_pause_in_slp:     1;
            uint32_t cntl_wdt_appcpu_reset_en:  1;
            uint32_t cntl_wdt_procpu_reset_en:  1;
            uint32_t cntl_wdt_flashboot_mod_en: 1;
            uint32_t cntl_wdt_sys_reset_length: 3;
            uint32_t cntl_wdt_cpu_reset_length: 3;
            uint32_t cntl_wdt_level_int_en:     1;
            uint32_t cntl_wdt_edge_int_en:      1;
            uint32_t cntl_wdt_stg3:             3;
            uint32_t cntl_wdt_stg2:             3;
            uint32_t cntl_wdt_stg1:             3;
            uint32_t cntl_wdt_stg0:             3;
            uint32_t cntl_wdt_en:               1;
        };
        uint32_t value;
    } cntl_wdtconfig0;
    union
    {
        struct
        {
            uint32_t stg0_hold: 32;
        };
        uint32_t value;
    } cntl_wdtconfig1;
    union
    {
        struct
        {
            uint32_t stg1_hold: 32;
        };
        uint32_t value;
    } cntl_wdtconfig2;
    union
    {
        struct
        {
            uint32_t stg2_hold: 32;
        };
        uint32_t value;
    } cntl_wdtconfig3;
    union
    {
        struct
        {
            uint32_t stg3_hold: 32;
        };
        uint32_t value;
    } cntl_wdtconfig4;
    union
    {
        struct
        {
            uint32_t :          31;
            uint32_t feed:      1;
        };
        uint32_t value;
    } cntl_wdtfeed;
    union
    {
        struct
        {
            uint32_t wkey:  32;
        };
        uint32_t value;
    } cntl_wdtwprotect;
    union
    {
        struct
        {
            uint32_t :          29;
            uint32_t ent_rtc:   1;
            uint32_t dtest_rtc: 2;
        };
        uint32_t value;
    } cntl_test_mux;
    union
    {
        struct
        {
            uint32_t :                      20;
            uint32_t sw_stall_appcpu_c1:    6;
            uint32_t sw_stall_procpu_c1:    6;      /* (R/W) reg_rtc_cntl_sw_stall_appcpu_c1[5:0],
                                                             reg_rtc_cntl_sw_stall_appcpu_c0[1:0] == 0x86 (100001 10)
                                                             will stall APP_CPU, see also RTC_CNTL_OPTIONS0_REG */
        };
        uint32_t value;
    } cntl_sw_cpu_stall;
    union
    {
        struct
        {
            uint32_t store;     /* (R/W) 32-bit general-purpose retention register */
        };
        uint32_t value;
    } cntl_store4;
    union
    {
        struct
        {
            uint32_t store;     /* (R/W) 32-bit general-purpose retention register */
        };
        uint32_t value;
    } cntl_store5;
    union
    {
        struct
        {
            uint32_t store;     /* (R/W) 32-bit general-purpose retention register */
        };
        uint32_t value;
    } cntl_store6;
    union
    {
        struct
        {
            uint32_t store;     /* (R/W) 32-bit general-purpose retention register */
        };
        uint32_t value;
    } cntl_store7;
    // TODO
} rtc_io_t;

extern rtc_io_t RTC;

#ifdef __cplusplus
}
#endif
