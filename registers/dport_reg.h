//
// Created by michal on 8.4.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef volatile struct
{
    union
    {
        struct
        {
            uint32_t boot_remap:    1;
            uint32_t :              31;
        };
        uint32_t value;
    } boot_remap_ctrl[2];       // 0x000
    union
    {
        struct
        {
            uint32_t access_check_pro:  1;
            uint32_t :                  7;
            uint32_t access_check_app:  1;
        };
        uint32_t value;
    } access_check;             // 0x008
    uint32_t pro_dport_apb_mask0;   // 0x00C
    uint32_t pro_dport_apb_mask1;   // 0x010
    uint32_t app_dport_apb_mask0;   // 0x014
    uint32_t app_dport_apb_mask1;   // 0x018
    union
    {
        struct
        {
            uint32_t AES_accelerator:   1;
            uint32_t SHA_accelerator:   1;
            uint32_t RSA_accelerator:   1;
            uint32_t secure_boot:       1;
            uint32_t digital_signature: 1;
            uint32_t :                  27;
        };
        uint32_t value;
    } peri_clk_en;
    union
    {
        struct
        {
            uint32_t AES_accelerator:   1;
            uint32_t SHA_accelerator:   1;
            uint32_t RSA_accelerator:   1;
            uint32_t secure_boot:       1;  // AES Accelerator and SHA Accelerator will also be reset
            uint32_t digital_signature: 1;  // AES Accelerator and RSA Accelerator will also be reset
            uint32_t :                  27;
        };
        uint32_t value;
    } peri_rst_en;
    uint32_t wifi_bb_cfg;
    uint32_t wifi_bb_cfg2;
    union
    {
        struct
        {
            uint32_t appcpu_resetting:  1;
            uint32_t :                  31;
        };
        uint32_t value;
    } appcpu_ctrl_reg_a;
    union
    {
        struct
        {
            uint32_t appcpu_clkgate_en: 1;
            uint32_t :                  31;
        };
        uint32_t value;
    } appcpu_ctrl_reg_b;
    union
    {
        struct
        {
            uint32_t appcpu_runstall:   1;
            uint32_t :                  31;
        };
        uint32_t value;
    } appcpu_ctrl_reg_c;
    union
    {
        struct
        {
            uint32_t appcpu_boot_address:   32;
        };
        uint32_t value;
    } appcpu_ctrl_reg_d;
    union
    {
        struct
        {
            uint32_t cpu_period_sel:    2;
            uint32_t lowspeed_clk_sel:  1;
            uint32_t fast_clk_rtc_sel:  1;
            uint32_t :                  28;
        };
        uint32_t value;
    } cpu_per_conf;
    union
    {
        struct
        {
            uint32_t :                          2;
            uint32_t mode:                      1;
            uint32_t enable:                    1;
            uint32_t flush_enable:              1;
            uint32_t flush_done:                1;
            uint32_t lock0_en:                  1;
            uint32_t lock1_en:                  1;
            uint32_t lock2_en:                  1;
            uint32_t lock3_en:                  1;
            uint32_t pro_single_iram_ena:       1;
            uint32_t pro_dram_split:            1;
            uint32_t pro_ahb_spi_req:           1;
            uint32_t pro_slave_req:             1;
            uint32_t ahb_spi_req:               1;
            uint32_t slave_req:                 1;
            uint32_t pro_dram_hl:               1;
            uint32_t :                          15;
        };
        uint32_t value;
    } pro_cache_ctrl;
    union
    {
        struct
        {
            uint32_t mask_iram0:            1;
            uint32_t mask_iram1:            1;
            uint32_t mask_irom:             1;
            uint32_t mask_dram1:            1;
            uint32_t mask_drom0:            1;
            uint32_t mask_opsdram:          1;
            uint32_t mmu_sram_page_mode:    3;
            uint32_t mmu_flash_page_mode:   2;
            uint32_t mmu_force_on:          1;
            uint32_t mmu_pd:                1;
            uint32_t mmu_ia_clr:            1;
            uint32_t :                      18;
        };
        uint32_t value;
    } pro_cache_ctrl1;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } pro_cache_lock_0_addr;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } pro_cache_lock_1_addr;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } pro_cache_lock_2_addr;
    union
    {
        struct
        {
            // TODO
        };
        uint32_t value;
    } pro_cache_lock_3_addr;
    union
    {
        struct
        {
            uint32_t :                          2;
            uint32_t mode:                      1;
            uint32_t enable:                    1;
            uint32_t flush_enable:              1;
            uint32_t flush_done:                1;
            uint32_t lock0_en:                  1;
            uint32_t lock1_en:                  1;
            uint32_t lock2_en:                  1;
            uint32_t lock3_en:                  1;
            uint32_t app_single_iram_ena:       1;
            uint32_t app_dram_split:            1;
            uint32_t app_ahb_spi_req:           1;
            uint32_t app_slave_req:             1;
            uint32_t app_dram_hl:               1;
            uint32_t :                          15;
        };
        uint32_t value;
    } app_cache_ctrl;
    union
    {
        struct
        {
            uint32_t mask_iram0:                1;
            uint32_t mask_iram1:                1;
            uint32_t mask_irom0:                1;
            uint32_t mask_dram1:                1;
            uint32_t mask_drom0:                1;
            uint32_t mask_opsdram:              1;
            uint32_t mmu_sram_page_mode:        3;
            uint32_t mmu_flash_page_mode:       2;
            uint32_t mmu_force_on:              1;
            uint32_t mmu_pd:                    1;
            uint32_t mmu_ia_clr:                1;
            uint32_t :                          18;
        };
        uint32_t value;
    } app_cache_ctrl1;




    union
    {
        struct
        {
            uint32_t cache_mux_mode:    2;  // The mode of the two caches sharing the memory. (R/W)
            uint32_t reserved:          30;
        };
        uint32_t value;
    } cache_mux_mode;
    union
    {
        struct
        {
            uint32_t reserved1:         1;
            uint32_t immu_page_mode:    2;  // Page size in the MMU for the internal SRAM 0. (R/W)
            uint32_t reserved2:         29;
        };
        uint32_t value;
    } immu_page_mode;
    union
    {
        struct
        {
            uint32_t reserved1:         1;
            uint32_t dmmu_page_mode:    2;
            uint32_t reserved2:         29;
        };
        uint32_t value;
    } dmmu_page_mode;
    union
    {
        struct
        {
            uint32_t reserved:  32; // TODO
        };
        uint32_t value;
    } sram_pd_ctrl_reg_0;
    union
    {
        struct
        {

        };
    } sram_pd_1;
} dport_t;

extern dport_t DPORT;

// TODO
//#define DPORT (asm("nop"); dport_workaround)

// TODO IMMU_PAGE_MODE = 1
// TODO DMMU_PAGE_MODE = 1

#ifdef __cplusplus
}
#endif