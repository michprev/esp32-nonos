//
// Created by michal on 29.4.18.
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

// TODO DPORT workaround

/**
  * @brief Initialise cache mmu, mark all entries as invalid.
  *
  * @param  int cpu_no : 0 for PRO cpu, 1 for APP cpu.
  *
  * @return None
  */
void ROM_mmu_init(int cpu_no);

/**
  * @brief Set Flash-Cache mmu mapping.
  *
  * @param  int cpu_no : CPU number, 0 for PRO cpu, 1 for APP cpu.
  *
  * @param  int pod : process identifier. Range 0~7.
  *
  * @param  unsigned int vaddr : virtual address in CPU address space.
  *                              Can be IRam0, IRam1, IRom0 and DRom0 memory address.
  *                              Should be aligned by psize.
  *
  * @param  unsigned int paddr : physical address in Flash.
  *                              Should be aligned by psize.
  *
  * @param  int psize : page size of flash, in kilobytes. Should be 64 here.
  *
  * @param  int num : pages to be set.
  *
  * @return unsigned int: error status
  *                   0 : mmu set success
  *                   1 : vaddr or paddr is not aligned
  *                   2 : pid error
  *                   3 : psize error
  *                   4 : mmu table to be written is out of range
  *                   5 : vaddr is out of range
  */
extern unsigned int ROM_cache_flash_mmu_set(int cpu_no, int pid, unsigned int vaddr, unsigned int paddr,  int psize, int num);

/**
  * @brief Flush the cache value for the cpu.
  *
  * @param  int cpu_no : 0 for PRO cpu, 1 for APP cpu.
  *
  * @return None
  */
extern void ROM_Cache_Flush(int cpu_no);

/**
  * @brief Disable Cache access for the cpu.
  *
  * @param  int cpu_no : 0 for PRO cpu, 1 for APP cpu.
  *
  * @return None
  */
extern void ROM_Cache_Read_Disable(int cpu_no);

/**
  * @brief Enable Cache access for the cpu.
  *
  * @param  int cpu_no : 0 for PRO cpu, 1 for APP cpu.
  *
  * @return None
  */
extern void ROM_Cache_Read_Enable(int cpu_no);

/**
  * @brief Initialise cache access for the cpu.
  *
  * @param  int cpu_no : 0 for PRO cpu, 1 for APP cpu.
  *
  * @return None
  */
extern void ROM_Cache_Read_Init(int cpu_no);

/**
  * @brief Set Ext-SRAM-Cache mmu mapping.
  *
  * @param  int cpu_no : CPU number, 0 for PRO cpu, 1 for APP cpu.
  *
  * @param  int pod : process identifier. Range 0~7.
  *
  * @param  unsigned int vaddr : virtual address in CPU address space.
  *                              Can be IRam0, IRam1, IRom0 and DRom0 memory address.
  *                              Should be aligned by psize.
  *
  * @param  unsigned int paddr : physical address in Ext-SRAM.
  *                              Should be aligned by psize.
  *
  * @param  int psize : page size of flash, in kilobytes. Should be 32 here.
  *
  * @param  int num : pages to be set.
  *
  * @return unsigned int: error status
  *                   0 : mmu set success
  *                   1 : vaddr or paddr is not aligned
  *                   2 : pid error
  *                   3 : psize error
  *                   4 : mmu table to be written is out of range
  *                   5 : vaddr is out of range
  */
extern unsigned int ROM_cache_sram_mmu_set(int cpu_no, int pid, unsigned int vaddr, unsigned int paddr, int psize, int num);

#ifdef __cplusplus
}
#endif