//
// Created by michal on 28.4.18.
//

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif



/**
  * @brief Get CRC for Fast RTC Memory.
  *
  * @param  uint32_t start_addr : 0 - 0x7ff for Fast RTC Memory.
  *
  * @param  uint32_t crc_len : 0 - 0x7ff, 0 for 4 byte, 0x7ff for 0x2000 byte.
  *
  * @return uint32_t : CRC32 result
  */
uint32_t calc_rtc_memory_crc(uint32_t start_addr, uint32_t crc_len);




extern uint8_t ROM_internal_i2c_readReg(uint8_t block, uint8_t host_id, uint8_t reg_add);

extern uint8_t ROM_internal_i2c_readReg_Mask(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t msb, uint8_t lsb);

extern void ROM_internal_i2c_writeReg(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t data);

extern void ROM_internal_i2c_writeReg_Mask(uint8_t block, uint8_t host_id, uint8_t reg_add, uint8_t msb, uint8_t lsb, uint8_t data);







extern void ROM_ets_delay_us(uint32_t us);

extern int ROM_ets_printf(const char *fmt, ...);





#ifdef __cplusplus
}
#endif