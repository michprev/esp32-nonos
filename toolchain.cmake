set(CMAKE_SYSTEM_NAME Generic)

set(CMAKE_C_COMPILER $ENV{XTENSA_TOOLCHAIN}/bin/xtensa-esp32-elf-gcc)
set(CMAKE_CXX_COMPILER $ENV{XTENSA_TOOLCHAIN}/bin/xtensa-esp32-elf-g++)
set(CMAKE_ASM_COMPILER $ENV{XTENSA_TOOLCHAIN}/bin/xtensa-esp32-elf-gcc)

# TODO -mfix-esp32-psram-cache-issue

set(CMAKE_C_FLAGS "-nostdlib -mlongcalls -ffunction-sections -fdata-sections -fstrict-volatile-bitfields" CACHE STRING "" FORCE)
set(CMAKE_CXX_FLAGS "-nostdlib -mlongcalls -ffunction-sections -fdata-sections -fstrict-volatile-bitfields" CACHE STRING "" FORCE)